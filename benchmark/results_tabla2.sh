#!/bin/bash

echo "Output to ${1:-results2.tex}"

FEM=gear-spots.vtu
SPOT=gear-fem.vtu

RadiusA="0.05"
ScaleA="0.101"

RadiusB="0.10"
ScaleB="0.201"

RadiusC="0.15"
ScaleC="0.301"

echo "% for radius = $RadiusA step 0.05 using $FEM searching points and $SPOT querying points" > ${1:-results2.tex}

echo "\multirow{3}{*}{"$RadiusA"}" >> ${1:-results2.tex}
./benchmark_santy --fems $FEM --spots $SPOT --radius $RadiusA --scale $ScaleA --latex  >> ${1:-results2.tex} 
echo "\hline" >> ${1:-results2.tex}

echo "\multirow{3}{*}{"$RadiusB"}" >> ${1:-results2.tex}
./benchmark_santy --fems $FEM --spots $SPOT --radius $RadiusB --scale $ScaleB --latex  >> ${1:-results2.tex} 
echo "\hline" >> ${1:-results2.tex}

echo "\multirow{3}{*}{"$RadiusC"}" >> ${1:-results2.tex}
./benchmark_santy --fems $FEM --spots $SPOT --radius $RadiusC --scale $ScaleC --latex  >> ${1:-results2.tex} 
echo "\hline" >> ${1:-results2.tex}
