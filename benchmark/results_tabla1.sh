#!/bin/bash

echo "Output to ${1:-results1.tex}"

FEM=gear-fem.vtu
SPOT=gear-spots.vtu

RadiusA="0.10"
ScaleA="0.201"

RadiusB="0.15"
ScaleB="0.301"

RadiusC="0.20"
ScaleC="0.401"

echo "% for radius = 0.05 step 0.05 using $FEM searching points and $SPOT querying points" > ${1:-results1.tex}

echo "\multirow{3}{*}{"$RadiusA"}" >> ${1:-results1.tex}
./benchmark_santy --fems $FEM --spots $SPOT --radius $RadiusA --scale $ScaleA --latex  >> ${1:-results1.tex} 
echo "\hline" >> ${1:-results1.tex}

echo "\multirow{3}{*}{"$RadiusB"}" >> ${1:-results1.tex}
./benchmark_santy --fems $FEM --spots $SPOT --radius $RadiusB --scale $ScaleB --latex  >> ${1:-results1.tex} 
echo "\hline" >> ${1:-results1.tex}

echo "\multirow{3}{*}{"$RadiusC"}" >> ${1:-results1.tex}
./benchmark_santy --fems $FEM --spots $SPOT --radius $RadiusC --scale $ScaleC --latex  >> ${1:-results1.tex} 
echo "\hline" >> ${1:-results1.tex}
