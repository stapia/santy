/****************************************************************************
 * (c) 2013 Copyright Santiago Tapia-Fernández (UPM)
 *
 * This file is part of Santy Library.
 *
 *     Santy Library is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Santy Library is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Santy Library.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#include "tbb/parallel_for.h"
using namespace tbb;

#include "test/point.hpp"
#include "santy/combine_neighborhood.hpp"
typedef santy::combine_neighborhood<Point3d, Point3d> Neighborhood;

#include "santy/par_combine_neighborhood.hpp"
typedef santy::par_combine_neighborhood<Point3d> ParNeighborhood;

#include <chrono>
#include <vector>

void move_the_points( std::vector<Point3d>& v );

typedef std::chrono::high_resolution_clock Clock;
using namespace std::chrono;

void evaluate_combine_santy(const std::vector<Point3d>& v, const std::vector<Point3d>& w, double radius, double scale, time_point<Clock> times[6])
{
    std::vector<Point3d> the_points(v);
    std::vector<Point3d> looking_for(w);

    {
        // Block to force objects destruction
        times[0] = Clock::now();

        Neighborhood domain(scale, the_points.size());
        domain.insert( the_points.begin(), the_points.end() );
        domain.insert_look_for( looking_for.begin(), looking_for.end() );
        //domain.print_size(std::cout);

        times[1] = Clock::now();

        domain.apply(radius);

        times[2] = Clock::now();

        move_the_points(the_points);
        domain.moved();

        times[3] = Clock::now();

        domain.apply(radius);

        times[4] = Clock::now();
    }

    times[5] = Clock::now();
}

void eval_par_combine_santy(const std::vector<Point3d>& v, const std::vector<Point3d>& w, double radius, double scale)
{
    printf("%50s","Parallel combine neighborhood\t");

    std::vector<Point3d> the_points(v);
    std::vector<Point3d> looking_for(w);

    time_point<Clock> times[6];

    {
        // Block to force objects destruction
        times[0] = Clock::now();

        ParNeighborhood domain(radius, scale);
        domain.insert( the_points.begin(), the_points.end() );
        domain.insert_look_for( looking_for.begin(), looking_for.end() );
        //domain.print_size(std::cout);

        times[1] = Clock::now();

        santy::IteratorRange<ParNeighborhood::SHashMap_Iterator> r(domain.searching.begin(), domain.searching.end());

        parallel_for( r, domain);

        times[2] = Clock::now();

        move_the_points(the_points);
        domain.moved();

        times[3] = Clock::now();

        santy::IteratorRange<ParNeighborhood::SHashMap_Iterator> rr(domain.searching.begin(), domain.searching.end());
        parallel_for( rr, domain);

        times[4] = Clock::now();
    }

    times[5] = Clock::now();

    for ( int i; i < 5; ++i )
    {
        duration<double> span = duration_cast<duration<double> >(times[i+1] - times[i]);
        printf("%10.1f  ", span.count() * 1000);
    }

    std::cout << "\n";
}

