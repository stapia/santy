/****************************************************************************
 * (c) 2013 Copyright Santiago Tapia-Fernández (UPM)
 *
 * This file is part of Santy Library.
 *
 *     Santy Library is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Santy Library is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Santy Library.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#include "program_options.hpp"

#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>

program_options::program_options(int argc, char** argv)
{
    //int verbose_flag = 0;
    int i;
    for ( i = 0; i < DOING_END; ++i )
    {
        doing[i] = 1;
    }
    doing[PAR_SANTY] = 0;

    point_number = 100000;
    random_seed = 4534;
    looking_for_number = 10000;
    radius = 1.6;
    scale = 1.0;
    rand_range = 10.0;
    latex = 0;
    stats = 0;

    fems = NULL;
    spots = NULL;

    int c;

    struct option my_long_options[] =
    {
        /* These options set a flag. */
        //{"verbose", no_argument,       &verbose_flag, 1},
        //{"brief",   no_argument,       &verbose_flag, 0},
        {"skip-ann",          no_argument, doing+ANN, 0},
        {"par-santy",         no_argument, doing+PAR_SANTY, 1},
        {"skip-santy",        no_argument, doing+SANTY, 0},
        {"skip-can",          no_argument, doing+COMBINE_SANTY, 0},
        {"latex",             no_argument, &latex, 1},
        {"stats",             no_argument, &stats, 1},
        /* These options don’t set a flag.
           We distinguish them by their indices. */
        {"seed",       required_argument, 0, 'x' },
        {"rand-range", required_argument, 0, 'X' },
        {"scale",       required_argument, 0, 'z' },
        {"radius",     required_argument, 0, 'r' },
        {"fems",       required_argument, 0, 'f' },
        {"spots",      required_argument, 0, 'p' },
        {"searching",  required_argument, 0, 's' },
        {"querying",   required_argument, 0, 'q' },
        {0, 0, 0, 0}
    };

    int option_index = 0;

    while ( (c = getopt_long (argc, argv, "x:X:z:r:f:p:s:q:", my_long_options, &option_index) ) != -1 )
    {
        switch (c)
        {
        case 0:
            /* If this option set a flag, do nothing else now. */
            if (my_long_options[option_index].flag != 0)
                break;
            fprintf (stderr, "option %s", my_long_options[option_index].name);
            if (optarg)
                fprintf (stderr, " with arg %s", optarg);
            fprintf (stderr, "\n");
            break;

        case 'x':
            random_seed = atoi(optarg);
            fprintf (stderr, "random seed set to %i\n", random_seed);
            break;

        case 'X':
            rand_range = atof(optarg);
            fprintf (stderr, "random range set to %f\n", rand_range);
            break;

        case 'r':
            radius = atof(optarg);
            fprintf (stderr, "radius set to %f\n", radius);
            break;

        case 'z':
            scale = atof(optarg);
            fprintf (stderr, "scale set to %f\n", scale);
            break;

        case 'f':
            fems = optarg;
            fprintf (stderr, "reading searching points from `%s'\n", fems);
            break;

        case 'p':
            spots = optarg;
            fprintf (stderr, "reading querying points from `%s'\n", spots);
            break;

        case 's':
            point_number = atoi(optarg);
            fprintf (stderr, "Searching set number of points = %i\n", point_number);
            break;

        case 'q':
            looking_for_number = atoi(optarg);
            fprintf (stderr, "Querying set number of points = %i\n", looking_for_number);
            break;

        case '?':
            /* getopt_long already printed an error message. */
            break;

        default:
            abort ();
        }
    }

    /* Print any remaining command line arguments (not options). */
    if (optind < argc)
    {
        fprintf (stderr, "non-option ARGV-elements: ");
        while (optind < argc)
            fprintf (stderr, "%s ", argv[optind++]);
        fprintf (stderr, "\n");
    }
}


