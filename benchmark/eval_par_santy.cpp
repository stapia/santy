/****************************************************************************
 * (c) 2013 Copyright Santiago Tapia-Fernández (UPM)
 *
 * This file is part of Santy Library.
 *
 *     Santy Library is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Santy Library is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Santy Library.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#include "test/point.hpp"
#include "santy/neighborhood.hpp"
#include "santy/details/replace_iterator.hpp"

#include <chrono>
#include <vector>

#include "tbb/parallel_do.h"
#include "tbb/parallel_for.h"
#include "tbb/blocked_range.h"
#include "tbb/parallel_reduce.h"

using namespace tbb;

void move_the_points( std::vector<Point3d>& v );

typedef std::chrono::high_resolution_clock Clock;
using namespace std::chrono;

typedef santy::neighborhood<Point3d> Neighborhood;

struct par_using_santy
{
    const double dist_;
    const Neighborhood& domain;

    par_using_santy( const Neighborhood& d, double dist )
        : domain( d )
        , dist_ (dist)
    {
    }

    void operator()( const Point3d& p ) const
    {
        std::vector<const Point3d*> par_near_p(20);
        domain.neighbors(p, dist_, santy::details::replacer(par_near_p, par_near_p.begin()));
    }

    void execute(const std::vector<Point3d>& looking_for )
    {
        parallel_do( looking_for.begin(), looking_for.end(), *this);
    }
};

struct par_using_santy2
{
    const double dist_;
    const Neighborhood& domain;
    const std::vector<Point3d>& looking_for;

    par_using_santy2( const Neighborhood& d, const std::vector<Point3d>& w, double dist )
        : domain( d )
        , looking_for( w )
        , dist_ (dist)
    {
    }

    void operator() ( const blocked_range<size_t>& r ) const
    {
        size_t i; std::vector<const Point3d*> par_near_p(20);

        for( i = r.begin(); i != r.end(); ++i )
        {
            domain.neighbors(looking_for[i], dist_, santy::details::replacer(par_near_p, par_near_p.begin()));
        }
    }

    void execute(const std::vector<Point3d>&)
    {
        parallel_for(blocked_range<size_t>(0, looking_for.size()), *this);
    }
};

struct par_using_santy_apply
{
    const double dist_;
    const Neighborhood& domain;
    std::vector<Point3d>& looking_for;

    par_using_santy_apply( const Neighborhood& d, std::vector<Point3d>& w, double dist )
        : domain( d )
        , looking_for( w )
        , dist_ (dist)
    {
    }

    void operator() ( const blocked_range<size_t>& r ) const
    {
        size_t i;

        for( i = r.begin(); i != r.end(); ++i )
        {
            domain.apply2neighbors(looking_for[i], dist_);
        }
    }

    void execute(const std::vector<Point3d>&)
    {
        parallel_for(blocked_range<size_t>(0, looking_for.size()), *this);
    }
};

/*
struct par_using_santy3
{
    const double dist_;
    const using_santy& domain;
    const std::vector<Point3d>& looking_for;
    std::vector<Point3d> par_near_p;

    par_using_santy3( const using_santy& d, const std::vector<Point3d>& w, double dist )
        : domain( d )
        , looking_for( w )
        , par_near_p(60)
        , dist_ (dist)
    {
    }

    par_using_santy3( par_using_santy3& x, split )
        : domain( x.domain )
        , looking_for( x.looking_for )
        , par_near_p(60)
        , dist_(x.dist_)
    {
    }

    void operator() ( const blocked_range<size_t>& r )
    {
        size_t i;

        for( i = r.begin(); i != r.end(); ++i )
        {
            domain.find_points(looking_for[i], dist_, par_near_p);
        }
    }

    void join( par_using_santy3& rhs )
    {
    }

    void execute(const std::vector<Point3d>&)
    {
        parallel_reduce( blocked_range<size_t>(0,looking_for.size()), *this );
    }
};
*/

void eval_par_santy(const std::vector<Point3d>& v, const std::vector<Point3d>& w, double radius, double scale)
{
    size_t number_of_exec = 1;
    //std::cout << "Number of executions for moving points: " << number_of_exec << "\n";

    std::vector<Point3d> the_points(v);
    std::vector<Point3d> looking_for(w);

    time_point<Clock> times[6];

    {
        times[0] = Clock::now();

        Neighborhood domain(scale);
        domain.insert( the_points.begin(), the_points.end() );

        //par_using_santy par(domain, radius);
        //par_using_santy2 par(domain, looking_for, radius);
        //par_using_santy3 par(domain, looking_for, radius);
        par_using_santy_apply par(domain, looking_for, radius);

        times[1] = Clock::now();

        par.execute(looking_for);

        times[2] = Clock::now();

        for ( int j = 0; j < number_of_exec; ++j )
        {
            move_the_points(the_points);
            move_the_points(looking_for);
            domain.moved();
            par.execute(looking_for);
        }

        times[3] = Clock::now();

        par.execute(looking_for);

        times[4] = Clock::now();
    }

    times[5] = Clock::now();

    printf("%50s","Parallel santy\t");

    for ( int i; i < 5; ++i )
    {
        duration<double> span = duration_cast<duration<double> >(times[i+1] - times[i]);
        printf("%10.1f  ", span.count() * 1000);
        if ( i == 2 )
        {
            printf("%10.1f  ", span.count() * 1000.0 / number_of_exec);
        }
    }

    std::cout << "\n";
}
