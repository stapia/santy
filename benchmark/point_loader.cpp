/****************************************************************************
 * (c) 2013 Copyright Santiago Tapia-Fernández (UPM)
 *
 * This file is part of Santy Library.
 *
 *     Santy Library is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Santy Library is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Santy Library.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#include "test/point.hpp"

#include <sys/stat.h>

#include <vector>
#include <cstdio>

#include <iostream>
#include <sstream>

#include <libxml/parser.h>
#include <libxml/tree.h>
#include <libxml/xpath.h>
#include <libxml/xpathInternals.h>

xmlDocPtr parserDoc(const char *content, int length)
{
    xmlDocPtr doc = xmlReadMemory(content, length, "points.xml", NULL, 0);
    if (doc == NULL)
    {
        fprintf(stderr, "Failed to parse document\n");
        return NULL;
    }
    return doc;
}

void read_vector( std::istream& reading, std::vector<Point3d>& v)
{
    int i; double a_point[3];
    for ( i = 0; i < 3; ++i ) { reading >> a_point[i]; }
    while ( reading.good() )
    {
        v.push_back( Point3d(a_point) );
        for ( i = 0; i < 3; ++i ) { reading >> a_point[i]; }
    }
}

void process_points(xmlNodeSetPtr nodes, std::vector<Point3d>& v)
{
    int size = (nodes) ? nodes->nodeNr : 0;
    if ( size == 1 )
    {
        xmlNodePtr p = nodes->nodeTab[0];

        std::string content((const char*)xmlNodeGetContent(p));

        std::stringstream reading(content);

        read_vector( reading, v);
    }
    else
    {
        fprintf(stderr, "Error: Points not found\n");
    }
}

int find_xpath_expression(xmlDocPtr doc, const xmlChar* xpathExpr, std::vector<Point3d>& v)
{
    xmlXPathContextPtr xpathCtx;
    xmlXPathObjectPtr xpathObj;

    /* Create xpath evaluation context */
    xpathCtx = xmlXPathNewContext(doc);
    if(xpathCtx == NULL)
    {
        fprintf(stderr,"Error: unable to create new XPath context\n");
        return(-1);
    }

    /* Evaluate xpath expression */
    xpathObj = xmlXPathEvalExpression(xpathExpr, xpathCtx);
    if(xpathObj == NULL)
    {
        fprintf(stderr,"Error: unable to evaluate xpath expression \"%s\"\n", xpathExpr);
        xmlXPathFreeContext(xpathCtx);
        return(-1);
    }

    // Read points
    process_points( xpathObj->nodesetval, v );

    /* Cleanup */
    xmlXPathFreeObject(xpathObj);
    xmlXPathFreeContext(xpathCtx);

    return(0);
}

void point_loader( const char* filename, std::vector<Point3d>& v )
{
    FILE* g; char *document; char *aux;
    struct stat st; size_t size;

    if ( stat(filename, &st) == -1 )
    {
        perror("stat");
        printf("in file: '%s'", filename);
        exit(EXIT_FAILURE);
    }

    stat(filename, &st);
    size = st.st_size;

    g = fopen(filename, "rb");
    document = (char*)malloc( size + 1 );

    size_t result = fread( document, sizeof(char), size, g );
    document[size] = '\0';

    if ( result != size )
    {
        fprintf(stderr,"Error: unable to read the file completely '%s'\n", filename);
    }
    else
    {
        fprintf(stderr,"Info: '%s' reading file completed\n", filename);
    }

    /* Process document VTKFile */
    xmlDoc *doc = NULL;
    xmlNode *root_element = NULL;

    doc = parserDoc(document, size);

    /*Get the root element node */
    root_element = xmlDocGetRootElement(doc);

    find_xpath_expression(doc, (const xmlChar*)"/VTKFile/UnstructuredGrid/Piece/Points/DataArray", v);

    std::cerr << "V: size = " << v.size() << "\n";
    std::cerr << v[0] << "\n";
    std::cerr << v[v.size()-1] << "\n";

    xmlFreeDoc(doc);

    xmlCleanupParser();
}

