#!/bin/bash

echo "Output to ${1:-results3.tex}"

FEM=iris-fem.vtu
SPOT=iris-spots.vtu

echo "% for radius = 0.15 step 0.15 using $FEM searching points and $SPOT querying points" > ${1:-results3.tex}

echo "\multirow{3}{*}{0.15}" >> ${1:-results3.tex}
./benchmark_santy --fems $FEM --spots $SPOT --radius 0.15 --scale 0.301 --latex  >> ${1:-results3.tex} 
echo "\hline" >> ${1:-results3.tex}

echo "\multirow{3}{*}{0.30}" >> ${1:-results3.tex}
./benchmark_santy --fems $FEM --spots $SPOT --radius 0.30 --scale 0.601 --latex  >> ${1:-results3.tex} 
echo "\hline" >> ${1:-results3.tex}

echo "\multirow{3}{*}{0.45}" >> ${1:-results3.tex}
./benchmark_santy --fems $FEM --spots $SPOT --radius 0.45 --scale 0.901 --latex  >> ${1:-results3.tex} 
echo "\hline" >> ${1:-results3.tex}
