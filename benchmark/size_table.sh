#!/bin/bash

echo "Output to ${1:-table-size.tex}"

echo "% Computing statistic for study cases" > ${1:-table-size.tex}

FEM=gear-fem.vtu
SPOT=gear-spots.vtu

RadiusA="0.10"
ScaleA="0.201"

RadiusB="0.15"
ScaleB="0.301"

RadiusC="0.20"
ScaleC="0.401"

echo "\multirow{4}{*}{Case 1}" >> ${1:-table-size.tex}

./benchmark_santy --fems $FEM --spots $SPOT --radius $RadiusA --scale $ScaleA --latex --stats >> ${1:-table-size.tex}  
./benchmark_santy --fems $FEM --spots $SPOT --radius $RadiusB --scale $ScaleB --latex --stats >> ${1:-table-size.tex}  
./benchmark_santy --fems $FEM --spots $SPOT --radius $RadiusC --scale $ScaleC --latex --stats >> ${1:-table-size.tex}  

echo "\hline" >> ${1:-table-size.tex}


FEM=gear-spots.vtu
SPOT=gear-fem.vtu

RadiusA="0.05"
ScaleA="0.101"

RadiusB="0.10"
ScaleB="0.201"

RadiusC="0.15"
ScaleC="0.301"

echo "\multirow{4}{*}{Case 2}" >> ${1:-table-size.tex}

./benchmark_santy --fems $FEM --spots $SPOT --radius $RadiusA --scale $ScaleA --latex --stats >> ${1:-table-size.tex}  
./benchmark_santy --fems $FEM --spots $SPOT --radius $RadiusB --scale $ScaleB --latex --stats >> ${1:-table-size.tex}  
./benchmark_santy --fems $FEM --spots $SPOT --radius $RadiusC --scale $ScaleC --latex --stats >> ${1:-table-size.tex}  

echo "\hline" >> ${1:-table-size.tex}

FEM=iris-fem.vtu
SPOT=iris-spots.vtu

echo "\multirow{3}{*}{Case 3}" >> ${1:-table-size.tex}

./benchmark_santy --fems $FEM --spots $SPOT --radius 0.15 --scale 0.301 --latex --stats  >> ${1:-table-size.tex} 
./benchmark_santy --fems $FEM --spots $SPOT --radius 0.30 --scale 0.601 --latex --stats  >> ${1:-table-size.tex} 
./benchmark_santy --fems $FEM --spots $SPOT --radius 0.45 --scale 0.901 --latex --stats  >> ${1:-table-size.tex} 

echo "\hline" >> ${1:-table-size.tex}

FEM=iris-spots.vtu
SPOT=iris-fem.vtu

echo "\multirow{3}{*}{Case 4}" >> ${1:-table-size.tex}

./benchmark_santy --fems $FEM --spots $SPOT --radius 0.10 --scale 0.201 --latex --stats  >> ${1:-table-size.tex} 
./benchmark_santy --fems $FEM --spots $SPOT --radius 0.15 --scale 0.301 --latex --stats  >> ${1:-table-size.tex} 
./benchmark_santy --fems $FEM --spots $SPOT --radius 0.20 --scale 0.401 --latex --stats  >> ${1:-table-size.tex} 

echo "\hline" >> ${1:-table-size.tex}

FEM=iris-fem.vtu
SPOT=iris-spots.vtu


