#!/bin/bash

echo "Output to ${1:-results4.tex}"

FEM=iris-spots.vtu
SPOT=iris-fem.vtu

echo "% for radius = 0.10 step 0.05 using $FEM searching points and $SPOT querying points" > ${1:-results4.tex}

echo "\multirow{3}{*}{0.10}" >> ${1:-results4.tex}
./benchmark_santy --fems $FEM --spots $SPOT --radius 0.10 --scale 0.201 --latex  >> ${1:-results4.tex} 
echo "\hline" >> ${1:-results4.tex}

echo "\multirow{3}{*}{0.15}" >> ${1:-results4.tex}
./benchmark_santy --fems $FEM --spots $SPOT --radius 0.15 --scale 0.301 --latex  >> ${1:-results4.tex} 
echo "\hline" >> ${1:-results4.tex}

echo "\multirow{3}{*}{0.20}" >> ${1:-results4.tex}
./benchmark_santy --fems $FEM --spots $SPOT --radius 0.20 --scale 0.401 --latex  >> ${1:-results4.tex} 
echo "\hline" >> ${1:-results4.tex}
