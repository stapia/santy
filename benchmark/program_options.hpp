/****************************************************************************
 * (c) 2013 Copyright Santiago Tapia-Fernández (UPM)
 *
 * This file is part of Santy Library.
 *
 *     Santy Library is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Santy Library is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Santy Library.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#pragma once

class program_options
{
public:
    program_options(int argc, char** argv);

    int random_seed;
    int point_number;
    int looking_for_number;
    int latex;
    int stats;

    double radius;
    double scale;
    double rand_range;

    const char* fems;
    const char* spots;

    enum { SANTY, COMBINE_SANTY, PAR_SANTY, ANN, DOING_END };
    int doing[DOING_END];

};

