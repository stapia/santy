
#pragma once

#include <ANN/ANN.h>

#include "test/point.hpp"
#include <vector>

class using_ann
{
public:
    using_ann(const std::vector<Point3d>&, size_t);
    ~using_ann();

    size_t apply_points(Point3d& p, double dist, const std::vector<Point3d>&, bool show = false);

    void moving_point(const std::vector<Point3d>&);

protected:
    enum { dim = 3 };

    size_t k;
    ANNpointArray dataPts; // data points
    int nPts; // actual number of data points
    ANNpoint queryPt; // query point
    ANNidxArray nnIdx; // near neighbor indices
    ANNdistArray dists; // near neighbor distances
    ANNkd_tree* kdTree; // search structure

};
