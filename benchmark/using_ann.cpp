#include "using_ann.hpp"

#include <cassert>

using_ann::using_ann(const std::vector<Point3d>& v, size_t average)
    : k(average)
{
    dataPts = annAllocPts(v.size(), dim); // allocate data points

    queryPt = annAllocPt(dim); // allocate query point
    nnIdx = new ANNidx[k]; // allocate near neigh indices
    dists = new ANNdist[k]; // allocate near neighbor dists

    int nPts = 0;
    for ( const auto& p : v )
    {
        for ( int j = 0; j < dim; ++j )
        {
            dataPts[nPts][j] = p[j];
        }
        ++nPts;
    }

    assert( nPts == v.size() );

    kdTree = new ANNkd_tree( // build search structure
        dataPts, // the data points
        nPts, // number of points
        dim); // dimension of space
}

using_ann::~using_ann()
{
    delete [] nnIdx; // clean things up
    delete [] dists;
    delete kdTree;
    annClose();
}

size_t using_ann::apply_points( Point3d& p, double dist, const std::vector<Point3d>& v, bool apply )
{
    for ( int j = 0; j < dim; ++j )
    {
        queryPt[j] = p[j];
    }

    int result;

    if ( apply )
    {
        result = kdTree->annkFRSearch(queryPt, dist*dist, k, nnIdx, dists, 0.0);

        for ( int i = 0; (i < k) && (i<result); ++i )
        {
            v[nnIdx[i]].apply( p );
        }
    }
    else
    {
        result = kdTree->annkFRSearch(queryPt, dist*dist, 0, nnIdx, dists, 0.0);
    }

    return result;
}

void using_ann::moving_point(const std::vector<Point3d>& v)
{
    delete kdTree;

    dataPts = annAllocPts(v.size(), dim); // allocate data points

    int nPts = 0;
    for ( const auto& p : v )
    {
        for ( int j = 0; j < dim; ++j )
        {
            dataPts[nPts][j] = p[j];
        }
        ++nPts;
    }

    assert( nPts == v.size() );

    kdTree = new ANNkd_tree( // build search structure
        dataPts, // the data points
        nPts, // number of points
        dim); // dimension of space
}

