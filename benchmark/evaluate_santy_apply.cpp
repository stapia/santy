/****************************************************************************
 * (c) 2013 Copyright Santiago Tapia-Fernández (UPM)
 *
 * This file is part of Santy Library.
 *
 *     Santy Library is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Santy Library is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Santy Library.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#include "test/point.hpp"
#include "santy/neighborhood.hpp"
typedef santy::neighborhood<Point3d> Neighborhood;

#include <chrono>
#include <vector>

void move_the_points( std::vector<Point3d>& v );

typedef std::chrono::high_resolution_clock Clock;
using namespace std::chrono;

void evaluate_santy_apply(const std::vector<Point3d>& v, const std::vector<Point3d>& w, double radius, double scale, time_point<Clock> times[6], int neighbours_size[3])
{
    size_t number_min = -1, number_max = 0, zero_neighbors = 0;

    std::vector<Point3d> the_points(v);
    std::vector<Point3d> looking_for(w);

    { // Block to force objects destruction
        times[0] = Clock::now();

        Neighborhood domain(scale, the_points.size());
        domain.insert(the_points.begin(), the_points.end());

        times[1] = Clock::now();

        for ( auto &p : looking_for )
        {
            size_t number = domain.apply2neighbors(p, radius);

            if ( number != 0 && number < number_min ) number_min = number;
            if ( number > number_max ) number_max = number;
            if ( number == 0 ) ++zero_neighbors;
        }

        times[2] = Clock::now();

        move_the_points(the_points);
        domain.moved();

        times[3] = Clock::now();

        for ( auto &p : looking_for )
        {
            size_t number = domain.apply2neighbors(p, radius);
        }

        times[4] = Clock::now();
    }

    times[5] = Clock::now();

    neighbours_size[0] = number_max;
    neighbours_size[1] = number_min;
    neighbours_size[2] = zero_neighbors;
}
