/****************************************************************************
 * (c) 2013 Copyright Santiago Tapia-Fernández (UPM)
 *
 * This file is part of Santy Library.
 *
 *     Santy Library is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Santy Library is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Santy Library.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#define __COUNTING__

#include "test/point.hpp"
double Point3d::range = 20.0;
#ifdef __COUNTING__
unsigned Point3d::number_of_apply = 0;
unsigned Point3d::number_of_distance = 0;
#endif // __COUNTING__

#include "using_ann.hpp"
#include "program_options.hpp"

#include <vector>
#include <chrono>

using namespace std::chrono;
typedef std::chrono::high_resolution_clock Clock;

void point_loader( const char* filename, std::vector<Point3d>& v );

void move_the_points( std::vector<Point3d>& v )
{
    for ( auto &p : v )
    {
        p[0] += 0.02;
    }
}

void print_row(time_point<Clock> times[6], int neighbours_size[3])
{
    for ( int i = 0; i < 5; ++i )
    {
        duration<double> span = duration_cast<duration<double> >(times[i+1] - times[i]);
        printf("%7.1f ", span.count() * 1000);
    }

    for ( int i = 0; i < 3; ++i )
    {
        printf("%7i ", neighbours_size[i]);
    }
#ifdef __COUNTING__
    printf("%7u ", Point3d::number_of_apply);
    printf("%7.3f\n",1.0*Point3d::number_of_apply/Point3d::number_of_distance);
#endif // NDEBUG
}

void print_latex_row(time_point<Clock> times[6])
{
    for ( int i = 0; i < 5; ++i )
    {
        duration<double> span = duration_cast<duration<double> >(times[i+1] - times[i]);
        printf(" & %8.1f ", span.count() * 1000);
    }
    std::cout << "\\\\\n";
}

void print_latex_stats(const program_options& options, int neighbours_size[3])
{
    printf("& %8.2f & ", options.radius);

    for ( int i = 0; i < 3; ++i )
    {
        printf("%7i & ", neighbours_size[i]);
    }
#ifdef __COUNTING__
    printf("%11u & ", Point3d::number_of_apply);
    printf("%11u & ", Point3d::number_of_distance);
    printf("%7.3f\\\\\n",1.0*Point3d::number_of_apply/Point3d::number_of_distance);
#endif // NDEBUG
}

void print_header()
{
    const char *label[] = {
        "Insert",
        "Neigh",
        "Move",
        "Neigh",
        "Clean",
        "Max",
        "Min",
        "zero",
        "Apply",
        "Ap/Dist"
    };

    // Print header
    printf("%16s"," ");
    int i;
    for ( i = 0; i < sizeof(label) / sizeof(char*); ++i )
    {
        printf("%7s ", label[i]);
    }
    printf("%s","\n");
}

void evaluate_combine_santy(const std::vector<Point3d>&, const std::vector<Point3d>&, double, double, time_point<Clock> times[6]);
void eval_par_combine_santy(const std::vector<Point3d>&, const std::vector<Point3d>&, double, double);
void evaluate_santy_apply(const std::vector<Point3d>&, const std::vector<Point3d>&, double, double, time_point<Clock> times[6], int neighbours_size[3]);
void eval_par_santy(const std::vector<Point3d>&, const std::vector<Point3d>&, double, double);
void evaluate_ann  (const std::vector<Point3d>&, const std::vector<Point3d>&, double, time_point<Clock> times[6], int neighbours_size[3], bool);

int main(int argc, char** argv)
{
    program_options options(argc, argv);

    int point_number = options.point_number;
    int random_seed = options.random_seed;
    int looking_for_number = options.looking_for_number;
    Point3d::range = options.rand_range;

    std::vector<Point3d> the_points;
    std::vector<Point3d> looking_for;

    if ( options.fems == NULL )
    {
        srand(random_seed+point_number);
        the_points.resize(point_number);
        std::cerr << "Generating " << point_number << " points randomly in a box of " << Point3d::range << "\n";
    }
    else
    {
        point_loader( options.fems, the_points);
    }

    if ( options.spots == NULL )
    {
        srand(random_seed+looking_for_number);
        looking_for.resize(looking_for_number);
        std::cerr << "Generating " << looking_for_number << " points randomly in a box of " << Point3d::range << "\n";
    }
    else
    {
        point_loader( options.spots, looking_for);
    }

    std::cerr << "Finding neighbors inside " << options.radius << " radius" << "\n";
    std::cerr << "Finding neighbors using " << options.scale << " scale" << "\n";
    std::cerr << "Searching set size = " << the_points.size() << " points\n";
    std::cerr << "Querying  set size = " << looking_for.size() << " points\n";

    time_point<Clock> times[6];
    int neighbours_size[3];
    if ( ! options.latex )
    {
        print_header();
    }

    if ( options.stats && options.latex )
    {
        // Only compute size of problem and exit
#ifdef __COUNTING__
        Point3d::number_of_apply = 0;
        Point3d::number_of_distance = 0;
#endif // NDEBUG

        evaluate_santy_apply(the_points, looking_for, options.radius, options.scale, times, neighbours_size);

        print_latex_stats(options, neighbours_size);

        return 0;
    }

    if ( options.doing[program_options::COMBINE_SANTY])
    {
#ifdef __COUNTING__
        Point3d::number_of_apply = 0;
        Point3d::number_of_distance = 0;
#endif // NDEBUG

        evaluate_combine_santy(the_points, looking_for, options.radius, options.scale, times);
        neighbours_size[0] = -1; neighbours_size[1] = -1; neighbours_size[2] = -1;
        if ( options.latex )
        {
            printf("& %10s","CANS");
            print_latex_row(times);
        }
        else
        {
            printf("%16s","santy CAN");
            print_row(times, neighbours_size);
        }
    }

    if ( options.doing[program_options::SANTY])
    {
#ifdef __COUNTING__
        Point3d::number_of_apply = 0;
        Point3d::number_of_distance = 0;
#endif // NDEBUG

        evaluate_santy_apply(the_points, looking_for, options.radius, options.scale, times, neighbours_size);
        if ( options.latex )
        {
            printf("& %10s","ANS");
            print_latex_row(times);
        }
        else
        {
            printf("%16s","santy neighbours");
            print_row(times, neighbours_size);
        }
    }

    if ( options.doing[program_options::ANN])
    {
#ifdef __COUNTING__
        Point3d::number_of_apply = 0;
        Point3d::number_of_distance = 1;
#endif // NDEBUG

        evaluate_ann(the_points, looking_for, options.radius, times, neighbours_size, false);
        if ( options.latex )
        {
            printf("& %10s","ANN");
            print_latex_row(times);
        }
        else
        {
            printf("%16s","ANN library");
            print_row(times, neighbours_size);
        }
#ifdef __COUNTING__
        Point3d::number_of_apply = 0;
        Point3d::number_of_distance = 1;
#endif // NDEBUG

        evaluate_ann(the_points, looking_for, options.radius, times, neighbours_size, true);
        if ( options.latex )
        {
            printf("& %10s","ANN-A");
            print_latex_row(times);
        }
        else
        {
            printf("%16s","ANN apply");
            print_row(times, neighbours_size);
        }
    }

    if ( options.doing[program_options::PAR_SANTY])
    {
        eval_par_santy(the_points, looking_for, options.radius, options.scale);
        eval_par_combine_santy(the_points, looking_for, options.radius, options.scale);
    }

    return 0;
}
