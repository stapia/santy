# README #

## Warning ##

We are working in this library no longer. We have used all the experience and background adquired in its development to produce a completely new version from scratch. Please, find the new library at [r2bt](https://bitbucket.org/stapia/r2bt).

## Summary ##

**Beta Version**

This is the library that implements the Combine Algorithm for Neighborhood as described in the following article:


S. Tapia-Fern ndez, I. Romero, A. Garcia-Beltran. Engineering with Computers, 2016, "A new approach for the solution of the neighborhood problem in meshfree methods" DOI 10.1007/s00366-016-0468-8


It is part of the a research project under development, it is not completed and its interface is under revision, but it is operational. Consider these facts before use in a real project.  

## Using the library ##

It is a header-only library. No need to compile, just include the top level headers in santy folder. 

There are two folders for the testing and benchmarking. Use CMake in order to compile them. You need some extra libraries to compile and import the example files in the data folder. Please read the CMakeLists.txt for the details, it is quite simple. 

## Installing ##

No need to install.

## Licence ##

The library is licence under GNU-GPL. See licence.txt

