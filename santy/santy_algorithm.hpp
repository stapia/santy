/****************************************************************************
 * (c) 2013 Copyright Santiago Tapia-Fernández (UPM)
 *
 * This file is part of Santy Library.
 *
 *     Santy Library is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Santy Library is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Santy Library.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#ifndef SANTY_ALGORITHM_HPP_2627637EG453GQ5T
#define SANTY_ALGORITHM_HPP_2627637EG453GQ5T

namespace santy
{
    template <typename Neighborhood, typename ApplyFunctor, typename Near2Functor>
    ApplyFunctor apply2neighbors( Neighborhood& neig, double radius, ApplyFunctor apply, Near2Functor near2)
    {
        typedef typename Neighborhood::Cell Cell;

        if ( radius < 0 )
        {
            throw std::runtime_error("radius could not be less than zero");
        }

        double ratio_radius_scale = radius / neig.scale - 0.5;
        long range = (ratio_radius_scale <= 0 ? 0 : 1+lrint( floor( ratio_radius_scale ) ));
        if ( range > 0 )
        {
            std::cerr << "Warning, ball radius is greater that 0.5*scale, algorithm speed will decrease" << "\n";
        }

        for ( auto& s_item : neig.searching )
        {
            const details::tessel &c = s_item.first;
            details::tessel aux;

            long x,y,z;

            for ( x = -range; x <= 1+range; ++x )
            {
                for ( y = -range; y <= 1+range; ++y )
                {
                    for ( z = -range; z <= 1+range; ++z )
                    {
                        aux.coor[0] = floor(c.coor[0] + x + 0.25);
                        aux.coor[1] = floor(c.coor[1] + y + 0.25);
                        aux.coor[2] = floor(c.coor[2] + z + 0.25);

                        auto found = neig.domain.find(aux);
                        if ( found != neig.domain.end() )
                        {
                            const Cell &cell = found->second;

                            for ( auto& i : cell )
                            {
                                for ( auto& p : s_item.second )
                                {
                                    if ( near2(p.get_point(), i.get_point()) )
                                    {
                                        apply ( i.get_point(), p.get_point() );
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return apply;
    }
}

#endif //SANTY_ALGORITHM_HPP_2627637EG453GQ5T
