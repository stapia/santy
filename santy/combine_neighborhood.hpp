/****************************************************************************
 * (c) 2013 Copyright Santiago Tapia-Fernández (UPM)
 *
 * This file is part of Santy Library.
 *
 *     Santy Library is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Santy Library is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Santy Library.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#ifndef SANTY_COMBINE_NEIGHBORHOOD_UNORDERED_MAP_HPP_0T3FVH
#define SANTY_COMBINE_NEIGHBORHOOD_UNORDERED_MAP_HPP_0T3FVH

#include <cmath>
#include <unordered_map>
#include <stdexcept>
#include <iostream>

#include "neighborhood.hpp"
#include "details/tessel.hpp"
#include "details/point.hpp"
#include "details/spoint.hpp"
#include "details/cell.hpp"
#include "details/scell.hpp"

namespace santy
{

template<typename Point3dQuerying, typename Point3dSearching>
class combine_neighborhood : public neighborhood<Point3dQuerying>
{

public:
    combine_neighborhood(double a_scale = 1.0, size_t size1 = 0, size_t size2 = 0)
        : neighborhood<Point3dQuerying>(a_scale, size1)
        , searching(size2)
    {
    }

    template<typename Iterator>
    void insert_look_for(Iterator first, Iterator last)
    {
        for ( ; first != last; ++first )
        {
            Point3dSearching& v = *first;
            details::spoint<Point3dSearching> p( this->scale, &v );
            insert_point_look_for(p);
        }
    }

    template<typename Iterator>
    void insert_pointers_look_for(Iterator first, Iterator last)
    {
        for ( ; first != last; ++first )
        {
            Point3dSearching* v = *first;
            details::spoint<Point3dSearching> p( this->scale, v );
            insert_point_look_for(p);
        }
    }

    void insert_point_look_for(const details::spoint<Point3dSearching> &p)
    {
        typename SHashMap::iterator found = searching.find(p.get_tessel());

        if ( found != searching.end() )
        {
            found->second.push_back(p);
        }
        else
        {
            SCell c;
            typename SHashMap::value_type x( p.get_tessel(), c);
            std::pair<typename SHashMap::iterator,bool> ok = searching.insert( x );
            if ( ok.second )
            {
                ok.first->second.push_back(p);
            }
            else
            {
                throw std::runtime_error("Cannot insert element in SHashMap?");
            }
        }

    }

    void apply(double radius)
    {
        if ( radius < 0 )
        {
            throw std::runtime_error("radius could not be less than zero");
        }

        double radius2 = radius * radius;
        double ratio_radius_scale = radius / this->scale - 0.5;
        long range = (ratio_radius_scale <= 0 ? 0 : 1+lrint( floor( ratio_radius_scale ) ));
        if ( range > 0 )
        {
            std::cerr << "Warning, ball radius is greater that 0.5*scale, algorithm speed will decrease" << "\n";
        }

        typename SHashMap::iterator si, se = searching.end();

        for ( si = searching.begin(); si != se; ++si )
        {
            const details::tessel &c = si->first;
            details::tessel aux;

            long x,y,z;

            for ( x = -range; x <= 1+range; ++x )
            {
                for ( y = -range; y <= 1+range; ++y )
                {
                    for ( z = -range; z <= 1+range; ++z )
                    {
                        aux.coor[0] = floor(c.coor[0] + x + 0.25);
                        aux.coor[1] = floor(c.coor[1] + y + 0.25);
                        aux.coor[2] = floor(c.coor[2] + z + 0.25);

                        typename HashMap::const_iterator found = this->domain.find(aux);
                        if ( found != this->domain.end() )
                        {
                            const Cell &cell = found->second;
                            typename Cell::const_iterator i, e = cell.end();

                            for ( i = cell.begin(); i != e; ++i )
                            {
                                for ( details::spoint<Point3dSearching>& p : si->second )
                                {
                                    if ( p.get_point().near2(i->get_point(), radius2) )
                                    {
                                        i->get_point().apply( p.get_point() );
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    void print(std::ostream& os)
    {
        int i = 1;
        for ( auto &a_pair : this->domain )
        {
            os << "Center: " << a_pair.first << " #Points: " << a_pair.second.size() << ( i % 4 == 0 ? "\n" : "\t" );
            ++i;
        }
    }

    void print_size(std::ostream& os)
    {
        os << "Domain size:\n";
        size_t num_tessel = 0, num_points = 0, num_empty_tessel = 0;
        for ( auto &a_pair : this->domain )
        {
            ++num_tessel;
            num_points += a_pair.second.size();
            if ( a_pair.second.empty() ) ++num_empty_tessel;
        }
        os << "#Center: " << num_tessel << " #Points: " << num_points
           << " #Empty Center: " << num_empty_tessel << "\n";
        os << "Search size:\n";
        num_tessel = 0, num_points = 0, num_empty_tessel = 0;
        for ( auto &a_pair : searching )
        {
            ++num_tessel;
            num_points += a_pair.second.size();
            if ( a_pair.second.empty() ) ++num_empty_tessel;
        }
        os << "#Center: " << num_tessel << " #Points: " << num_points
           << " #Empty Center: " << num_empty_tessel << "\n";
    }

//protected:

    typedef details::cell<Point3dQuerying> Cell;
    typedef details::scell<Point3dSearching> SCell;
    typedef details::tessel Tessel;

    typedef std::unordered_map<Tessel, Cell> HashMap;
    typedef std::unordered_map<Tessel, SCell> SHashMap;

    SHashMap searching;
};

} // namespace

#endif //SANTY_COMBINE_NEIGHBORHOOD_UNORDERED_MAP_HPP_0T3FVH
