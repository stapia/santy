/****************************************************************************
 * (c) 2013 Copyright Santiago Tapia-Fernández (UPM)
 *
 * This file is part of Santy Library.
 *
 *     Santy Library is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Santy Library is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Santy Library.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#ifndef SANTY_NEIGHBORHOOD_UNORDERED_MAP_HPP_0439A0F4UQ3FQUF0P49QAUZJKDFVH
#define SANTY_NEIGHBORHOOD_UNORDERED_MAP_HPP_0439A0F4UQ3FQUF0P49QAUZJKDFVH

#include <cmath>
#include <unordered_map>
#include <stdexcept>
#include <iostream>

#include "details/tessel.hpp"
#include "details/point.hpp"
#include "details/spoint.hpp"
#include "details/cspoint.hpp"
#include "details/cell.hpp"

namespace santy
{

template<typename Point3d>
class neighborhood
{

public:
    neighborhood(double a_scale = 1.0, size_t size = 0)
        : domain(size)
        , scale(a_scale)
    {
    }

    template<typename Iterator>
    void insert(Iterator first, Iterator last)
    {
        for ( ; first != last; ++first )
        {
            const Point3d& v = *first;
            details::point<Point3d> p( scale, &v );
            insert_point(p);
        }
    }

    template<typename Iterator>
    void insert_pointers(Iterator first, Iterator last)
    {
        for ( ; first != last; ++first )
        {
            const Point3d* v = *first;
            details::point<Point3d> p( scale, v );
            insert_point(p);
        }
    }

    void insert_point(const details::point<Point3d>& p)
    {
        typename HashMap::iterator found = domain.find(p.get_tessel());

        if ( found != domain.end() )
        {
            found->second.push_back(p);
        }
        else
        {
            Cell c;
            typename HashMap::value_type x( p.get_tessel(), c);
            std::pair<typename HashMap::iterator,bool> ok = domain.insert( x );
            if ( ok.second )
            {
                ok.first->second.push_back(p);
            }
            else
            {
                throw std::runtime_error("Cannot insert element in HashMap?");
            }
        }
    }

    size_t apply2neighbors(Point3d& v, double radius) const
    {
        size_t number = 0;

        if ( radius < 0 )
        {
            throw std::runtime_error("radius could not be less than zero");
        }

        details::spoint<Point3d> p( scale, &v );
        const details::tessel &c = p.get_tessel();
        details::tessel aux;

        double radius2 = radius*radius;
        double ratio_radius_scale = radius / scale - 0.5;
        long range = (ratio_radius_scale <= 0 ? 0 : 1+lrint( floor( ratio_radius_scale ) ));
        long x,y,z;

        for ( x = -range; x <= 1+range; ++x )
        {
            for ( y = -range; y <= 1+range; ++y )
            {
                for ( z = -range; z <= 1+range; ++z )
                {
                    aux.coor[0] = floor(c.coor[0] + x + 0.25);
                    aux.coor[1] = floor(c.coor[1] + y + 0.25);
                    aux.coor[2] = floor(c.coor[2] + z + 0.25);

                    typename HashMap::const_iterator found = domain.find(aux);
                    if ( found != domain.end() )
                    {
                        const Cell &cell = found->second;
                        typename Cell::const_iterator i, e = cell.end();

                        for ( i = cell.begin(); i != e; ++i )
                        {
                            if ( v.near2(i->get_point(), radius2) )
                            {
                                i->get_point().apply( v );
                                ++number;
                            }
                        }
                    }
                }
            }
        }

        return number;
    }

    template<typename OutputIterator>
    size_t neighbors(const Point3d& v, double radius, OutputIterator result) const
    {
        size_t number = 0;

        if ( radius < 0 )
        {
            throw std::runtime_error("radius could not be less than zero");
        }

        details::cspoint<Point3d> p( scale, &v );
        const details::tessel &c = p.get_tessel();
        details::tessel aux;

        double radius2 = radius*radius;
        double ratio_radius_scale = radius / scale - 0.5;
        long range = (ratio_radius_scale <= 0 ? 0 : 1+lrint( floor( ratio_radius_scale ) ));
        if ( range > 0 )
        {
            std::cerr << "Warning, ball radius is greater that 0.5*scale, algorithm speed will decrease" << "\n";
        }

        long x,y,z;

        for ( x = -range; x <= 1+range; ++x )
        {
            for ( y = -range; y <= 1+range; ++y )
            {
                for ( z = -range; z <= 1+range; ++z )
                {
                    aux.coor[0] = floor(c.coor[0] + x + 0.25);
                    aux.coor[1] = floor(c.coor[1] + y + 0.25);
                    aux.coor[2] = floor(c.coor[2] + z + 0.25);

                    typename HashMap::const_iterator found = domain.find(aux);
                    if ( found != domain.end() )
                    {
                        const Cell &cell = found->second;
                        typename Cell::const_iterator i, e = cell.end();

                        for ( i = cell.begin(); i != e; ++i )
                        {
                            if ( v.near2(i->get_point(), radius2) )
                            {
                                // Note: what shall be returned?
                                // - (answer 1) a reference, then coding should be:
                                *result = i->get_ppoint();
                                // - (answer 2) a copy of the point then:
                                //*result = i->get_point();
                                // Anyway advance inserter iterator
                                ++result;
                                ++number;
                            }
                        }
                    }
                }
            }
        }
        return number;
    }

    void moved()
    {
        std::list< details::unowned_cref< Point3d > > list_;

        for ( auto &a_pair : domain )
        {
            a_pair.second.eraser( list_, scale, a_pair.first );
        }

        insert( list_.begin(), list_.end() );
    }

    void print(std::ostream& os)
    {
        int i = 1;
        for ( auto &a_pair : domain )
        {
            os << "Center: " << a_pair.first << " #Points: " << a_pair.second.size() << ( i % 4 == 0 ? "\n" : "\t" );
            ++i;
        }
    }

    void print_size(std::ostream& os)
    {
        size_t num_tessel = 0, num_points = 0, num_empty_tessel = 0;
        for ( auto &a_pair : domain )
        {
            ++num_tessel;
            num_points += a_pair.second.size();
            if ( a_pair.second.empty() ) ++num_empty_tessel;
        }
        os << "#Center: " << num_tessel << " #Points: " << num_points
           << " #Empty Center: " << num_empty_tessel << "\n";
    }

//protected:

    typedef details::cell<Point3d> Cell;
    typedef details::tessel Tessel;
    //typedef details::tessel_hash TesselHash;
    typedef std::unordered_map<Tessel, Cell> HashMap;

    HashMap domain;
    double scale;

};

} // namespace

#endif //SANTY_NEIGHBORHOOD_UNORDERED_MAP_HPP_0439A0F4UQ3FQUF0P49QAUZJKDFVH
