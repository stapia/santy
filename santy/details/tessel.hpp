/****************************************************************************
 * (c) 2013 Copyright Santiago Tapia-Fernández (UPM)
 *
 * This file is part of Santy Library.
 *
 *     Santy Library is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Santy Library is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Santy Library.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#ifndef SANTY_TESSEL_HPP_2A4353QT45T
#define SANTY_TESSEL_HPP_2A4353QT45T

#include  <functional>

namespace santy
{
namespace details
{

class tessel
{
public:
    double coor[3];
    inline bool operator==(const tessel& x) const
    {
        return coor[0] == x.coor[0] &&
               coor[1] == x.coor[1] &&
               coor[2] == x.coor[2];
    }

    inline bool operator!=(const tessel& x) const
    {
        return ! ( *this == x );
    }
};

inline std::ostream& operator<<(std::ostream& os, const tessel& x)
{
    return os << x.coor[0] << " " << x.coor[1] << " " << x.coor[2];
}

} // namespace

} // namespace

namespace std
{
    template<> struct hash<santy::details::tessel>
    {
        typedef santy::details::tessel argument_type;
        typedef std::size_t result_type;
        result_type operator()(argument_type const& k) const
        {
            std::hash<double> hash_f;
            size_t h = 0;

            h =  hash_f(k.coor[0]);
            // Magic Number and Expression adapted from boost::hash_combine
            h ^= hash_f(k.coor[1]) + 0x9e3779b9 + (h<<6) + (h>>2);
            h ^= hash_f(k.coor[2]) + 0x9e3779b9 + (h<<6) + (h>>2);

            return h;
        }
    };
}

#endif //SANTY_TESSEL_HPP_2A4353QT45T
