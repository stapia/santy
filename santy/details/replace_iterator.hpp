/****************************************************************************
 * (c) 2013 Copyright Santiago Tapia-Fernández (UPM) & David Cortes (FFII)
 *
 * This file is part of Santy Library.
 *
 *     Santy Library is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Santy Library is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Santy Library.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#ifndef SANTY_DETAILS_REPLACE_ITERATOR_HPP__AQ2345Q45Q
#define SANTY_DETAILS_REPLACE_ITERATOR_HPP__AQ2345Q45Q

namespace santy
{

namespace details
{

template <class Container>
class replace_iterator
{
public:
    explicit replace_iterator( Container& c, typename Container::iterator i )
        : c_ ( std::addressof(c) )
        , actual_(i)
        , end_(c.end())
    {
    }

    replace_iterator<Container>&
    operator=( const typename Container::value_type& value )
    {
        if ( actual_ != end_ )
        {
            *actual_ = value;
        }
        else
        {
            actual_ = c_->insert(actual_, value);
            end_ = c_->end();
        }
        return *this;
    }

    replace_iterator<Container>&
    operator=( typename Container::value_type&& value )
    {
        if ( actual_ != end_ )
        {
            *actual_ = value;
        }
        else
        {
            actual_ = c_->insert(actual_, std::move(value) );
            end_ = c_->end();
        }
        return *this;
    }

    // Does nothing
    replace_iterator& operator*()
    {
        return *this;
    }

    replace_iterator& operator++()
    {
        if ( actual_ != end_ )
        {
            ++actual_;
        }
        return *this;
    }

    replace_iterator& operator++( int )
    {
        replace_iterator old = *this;
        ++(*this);
        return old;
    }

protected:
    typedef typename Container::iterator Iterator;
    Container *c_;
    Iterator actual_, end_;
};

template< class Container >
replace_iterator<Container> replacer( Container& c, typename Container::iterator i )
{
    return replace_iterator<Container>(c, i);
}

} //namespace

} //namespace

#endif //SANTY_DETAILS_REPLACE_ITERATOR_HPP__AQ2345Q45Q
