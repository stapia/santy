
/****************************************************************************
 * (c) 2015 Copyright Santiago Tapia-Fernández (UPM)
 *
 * This file is part of Santy Library.
 *
 *     Santy Library is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Santy Library is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Santy Library.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#ifndef SANTY_DETAILS_CELL_HPP_ERTQERTAERGF
#define SANTY_DETAILS_CELL_HPP_ERTQERTAERGF

#include <list>

#include "point.hpp"
#include "tessel.hpp"

namespace santy
{
namespace details
{

template <typename Point3d>
class cell : public std::list< point<Point3d> >
{
public:

    class Eraser
    {
    public:
        Eraser( std::list< unowned_cref< Point3d > >& the_list, const double& z)
            : list_(the_list)
            , scale(z)
        {
        }

        const double& scale;
        tessel c_;
        std::list< unowned_cref< Point3d > > &list_;

        void set_tessel( const tessel& c )
        {
            c_ = c;
        }

        bool operator() (point<Point3d>& value)
        {
            value.compute_tessel(scale);
            if ( value.get_tessel() != c_ )
            {
                unowned_cref<Point3d> x(value.get_point());
                list_.push_back( x );
                return true; //point will be removed
            }
            return false;
        }
    };

    void eraser( std::list< unowned_cref< Point3d > >& the_list, const double& z, const tessel& c)
    {
        Eraser eraser(the_list, z);
        eraser.set_tessel( c );
        this->remove_if( eraser );
    }

};

} //namespace
} //namespace

#endif

