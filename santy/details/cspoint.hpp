/****************************************************************************
 * (c) 2013 Copyright Santiago Tapia-Fernández (UPM)
 *
 * This file is part of Santy Library.
 *
 *     Santy Library is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Santy Library is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Santy Library.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#ifndef SANTY_CONST_SEARCH_POINT_HPP_34254525T
#define SANTY_CONST_SEARCH_POINT_HPP_34254525T

#include "unowned_ref.hpp"
#include "tessel.hpp"

namespace santy
{
namespace details
{

template <typename Point3d>
class cspoint
{
public:
    cspoint(double scale, const Point3d* pv = 0);

    inline const Point3d& get_point() const;
    inline const Point3d* get_ppoint() const;
    inline const tessel& get_tessel() const;
    inline void reset(const Point3d* pv);
    inline void compute_tessel(double scale);

private:

    tessel nearest;
    unowned_cref<Point3d> ref_point;

};

template<typename Point3d>
cspoint<Point3d>::cspoint(double scale, const Point3d* pv)
    : ref_point(pv)
{
    compute_tessel(scale);
}

template<typename Point3d>
const tessel& cspoint<Point3d>::get_tessel() const
{
    return nearest;
}

template<typename Point3d>
const Point3d& cspoint<Point3d>::get_point() const
{
    return ref_point.base();
}

template<typename Point3d>
const Point3d* cspoint<Point3d>::get_ppoint() const
{
    return ref_point.pbase();
}

template<typename Point3d>
void cspoint<Point3d>::reset(const Point3d* pv)
{
    ref_point = pv;
    compute_tessel();
}

template<typename Point3d>
void cspoint<Point3d>::compute_tessel(double scale)
{
    int i;
    for ( i = 0; i < 3; ++i )
    {
        const double &coor = ref_point.base()[i];
        nearest.coor[i] = floor(coor/scale - 0.5);
    }
}

} // namespace
} // namespace

#endif //SANTY_CONST_SEARCH_POINT_HPP_34254525T
