/****************************************************************************
 * (c) 2013 Copyright Santiago Tapia-Fernández (UPM) & David Cortes (FFII)
 *
 * This file is part of Santy Library.
 *
 *     Santy Library is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Santy Library is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Santy Library.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#ifndef SANTY_DETAILS_UNOWNED_REF_HPP__093458043TWGL
#define SANTY_DETAILS_UNOWNED_REF_HPP__093458043TWGL

namespace santy
{

namespace details
{

template <class Base>
class unowned_ref
{
public:
    unowned_ref(Base* b = 0) : ptr(b) { }
    unowned_ref(Base& b) : ptr(&b) { }

    unowned_ref& swap(unowned_ref & rhs)
    {
        std::swap(ptr, rhs.ptr);
        return *this;
    }

    template<typename Derived>
    unowned_ref & operator=(const Derived & rhs)
    {
        unowned_ref(rhs).swap(*this);
        return *this;
    }

    unowned_ref & operator=(Base & rhs)
    {
        ptr = & rhs;
        return *this;
    }

public: // queries

    inline bool empty() const { return !ptr; }

    inline Base& base() { return *ptr; }
    inline const Base& base() const { return *ptr; }

    inline operator Base&() { return *ptr; }
    inline operator const Base&() const { return *ptr; }

    inline Base* operator->() { return ptr; }
    inline const Base* operator->() const { return ptr; }
    inline Base* pbase() { return ptr; }
    inline const Base* pbase() const { return ptr; }

private:
    Base *ptr;
};

template <class Base>
class unowned_cref
{
public:
    unowned_cref(const Base& b) : ptr(&b) { }
    unowned_cref(const Base* b = 0) : ptr(b) { }

    inline const Base& base() const { return *ptr; }
    inline operator const Base&() const { return *ptr; }
    inline const Base* operator->() const { return ptr; }
    inline const Base* pbase() const { return ptr; }

private:
    const Base *ptr;
};

} //namespace

} //namespace

#endif //SANTY_DETAILS_UNOWNED_REF_HPP__093458043TWGL
