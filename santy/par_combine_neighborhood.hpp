/****************************************************************************
 * (c) 2013 Copyright Santiago Tapia-Fernández (UPM)
 *
 * This file is part of Santy Library.
 *
 *     Santy Library is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Santy Library is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Santy Library.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#ifndef SANTY_PAR_COMBINE_NEIGHBORHOOD_UNORDERED_MAP_HPP_ASFA
#define SANTY_PAR_COMBINE_NEIGHBORHOOD_UNORDERED_MAP_HPP_ASFA

#include <cmath>
#include <unordered_map>
#include <stdexcept>
#include <iostream>

#include "combine_neighborhood.hpp"
#include "details/tessel.hpp"
#include "details/point.hpp"
#include "details/spoint.hpp"
#include "details/cell.hpp"
#include "details/scell.hpp"

namespace santy
{

template <typename Iterator>
struct IteratorRange
{
    bool divisible;
    Iterator i, e;

    bool empty() const
    {
        return i == e;
    }

    bool is_divisible() const
    {
        return divisible;
    }

    IteratorRange( IteratorRange& r, split )
    {
        i = r.i;
        e = r.e;
        divisible = false;

        ++(r.i);
    }

    IteratorRange( Iterator first, Iterator last )
        : i(first)
        , e(last)
        , divisible(true)
    {
    }
};

template<typename Point3d>
class par_combine_neighborhood : public combine_neighborhood<Point3d, Point3d>
{

public:
    par_combine_neighborhood(double a_radius, double a_scale = 1.0, size_t size1 = 0, size_t size2 = 0)
        : combine_neighborhood<Point3d, Point3d>(a_scale, size1, size2)
        , radius(a_radius)
        , radius2(a_radius*a_radius)
    {
        if ( radius < 0 )
        {
            throw std::runtime_error("radius could not be less than zero");
        }

        double ratio_radius_scale = radius / this->scale - 0.5;
        range = (ratio_radius_scale <= 0 ? 0 : 1+lrint( floor( ratio_radius_scale ) ));
    }

    typedef details::cell<Point3d> Cell;
    typedef details::scell<Point3d> SCell;
    typedef details::tessel Tessel;

    typedef std::unordered_map<Tessel, Cell> HashMap;
    typedef std::unordered_map<Tessel, SCell> SHashMap;

    typedef typename SHashMap::iterator SHashMap_Iterator;
    typedef IteratorRange<SHashMap_Iterator> Range;

    void operator() ( const Range& r ) const
    {
        SHashMap_Iterator si = r.i;
        const details::tessel &c = si->first;
        details::tessel aux;

        long x,y,z;

        for ( x = -range; x <= 1+range; ++x )
        {
            for ( y = -range; y <= 1+range; ++y )
            {
                for ( z = -range; z <= 1+range; ++z )
                {
                    aux.coor[0] = floor(c.coor[0] + x + 0.25);
                    aux.coor[1] = floor(c.coor[1] + y + 0.25);
                    aux.coor[2] = floor(c.coor[2] + z + 0.25);

                    typename HashMap::const_iterator found = this->domain.find(aux);
                    if ( found != this->domain.end() )
                    {
                        const Cell &cell = found->second;
                        typename Cell::const_iterator i, e = cell.end();

                        for ( i = cell.begin(); i != e; ++i )
                        {
                            for ( details::spoint<Point3d>& p : si->second )
                            {
                                if ( p.get_point().near2(i->get_point(), radius2) )
                                {
                                    i->get_point().apply( p.get_point() );
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    long range;
    double radius;
    double radius2;
};

} // namespace

#endif //SANTY_PAR_COMBINE_NEIGHBORHOOD_UNORDERED_MAP_HPP_ASFA
