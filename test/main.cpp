/****************************************************************************
 * (c) 2013 Copyright Santiago Tapia-Fernández (UPM)
 *
 * This file is part of Santy Library.
 *
 *     Santy Library is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Santy Library is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Santy Library.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#include <chrono>
#include <iostream>
#include <vector>
#include <cstdlib>
#include <cstdio>
#include <algorithm>

#include "program_options.hpp"

#define __COUNTING__

#include "point.hpp"
double Point3d::range = 20.0;
#ifdef __COUNTING__
unsigned Point3d::number_of_apply = 0;
unsigned Point3d::number_of_distance = 0;
#endif // __COUNTING__

program_options options;

#include "santy/neighborhood.hpp"
#include "santy/combine_neighborhood.hpp"
#include "santy/santy_algorithm.hpp"
#include "santy/details/replace_iterator.hpp"

typedef santy::neighborhood<Point3d> Neighborhood;
typedef santy::combine_neighborhood<Point3d, Point3d> C_Neighborhood;
typedef santy::details::unowned_cref<Point3d> RefPoint;
typedef std::vector< RefPoint > RefPointVector;

using namespace santy::details;

size_t find_neighborhood(const Point3d& p, double dist, RefPointVector& vec, const Neighborhood& map)
{
    size_t number;

    if ( 0 )
    {
        // Using back_inserter ( new points are added to container )
        vec.clear(); // thus remove previously inserted points
        number = map.neighbors(p, dist, std::back_inserter(vec));
    }
    else
    {
        // Using my replace OutInserter (no need to clear), avoid call to destructor
        number = map.neighbors(p, dist, replacer(vec, vec.begin()));
    }

    if ( options.verbose )
    {
        std::cout << "--> " << number << " neighbours of " << p << " in distance < " << dist << ": \n";

        RefPointVector::const_iterator ite, e = vec.begin() + number;

        for ( ite = vec.begin(); ite != e; ++ite )
        {
            std::cout << *(ite) << " " << distance(*ite, p) << "\n";
        }
    }

    return number;
}

int regression_test(const Point3d& p, double dist, const RefPointVector& neig, const std::vector<Point3d>& domain)
{
    int fail = 0;
    for ( const Point3d &x : domain )
    {
        auto res = std::find_if( neig.begin(), neig.end(), [&x] (const RefPoint& r)
        {
            return x.label == r->label;
        } );

        double the_distance = distance(p, x);

        if ( the_distance < dist && res == neig.end() )
        {
            std::cerr << "Error these points are neighbors:\n"
                      << "\t" << p << "\n"
                      << "\t" << x << "\n"
                      << "\tDistance is " << distance(p, x) << "\n";
            fail = 1;
        }
        else if ( the_distance >= dist && res != neig.end() )
        {
            std::cerr << "Error these points are not neighbors:\n"
                      << "\t" << p << "\n"
                      << "\t" << *res << "\n"
                      << "\tDistance is " << distance(p, *res) << "\n";
            fail = 1;
        }
    }

    if ( fail )
    {
        auto ite = neig.begin(), e = neig.end();
        std::cerr << "Points in neighborhood of point " << p << " are: \n";
        for ( ; ite != e; ++ite )
        {
            std::cout << *(ite) << "\n";
        }
    }
    return fail;
}

int regression_test_pointers(const Point3d& p, double dist, const RefPointVector& neig, const std::vector<Point3d*>& domain)
{
    int fail = 0;
    for ( const Point3d *x : domain )
    {
        auto res = std::find_if( neig.begin(), neig.end(), [&x] (const RefPoint& r)
        {
            return x->label == r->label;
        } );

        double the_distance = distance(p, *x);

        if ( the_distance < dist && res == neig.end() )
        {
            std::cerr << "Error these points are neighbors:\n"
                      << "\t" << p << "\n"
                      << "\t" << *x << "\n"
                      << "\tDistance is " << distance(p, *x) << "\n";
            fail = 1;
        }
        else if ( the_distance >= dist && res != neig.end() )
        {
            std::cerr << "Error these points are not neighbors:\n"
                      << "\t" << p << "\n"
                      << "\t" << *res << "\n"
                      << "\tDistance is " << distance(p, *res) << "\n";
            fail = 1;
        }
    }

    if ( fail )
    {
        auto ite = neig.begin(), e = neig.end();
        std::cerr << "Points in neighborhood of point " << p << " are: \n";
        for ( ; ite != e; ++ite )
        {
            std::cout << *(ite) << "\n";
        }
    }
    return fail;
}

void test_performance()
{
    std::cout << "---- Begin Performance Test\n";
    typedef std::chrono::high_resolution_clock Clock;
    using namespace std::chrono;
    srand(options.random_seed);
    Neighborhood domain(options.scale);
    int i;

    std::vector<Point3d> v(options.point_number);

    auto t1 = Clock::now();

    domain.insert( v.begin(), v.end() );
    domain.print_size(std::cout);

    auto t2 = Clock::now();

    RefPointVector near_p(100);

    // Find point without moving
    int j;
    for ( j = 0; j < options.looking_for_number; ++j )
    {
        Point3d p;
        size_t number = find_neighborhood(p, options.radius, near_p, domain);
    }

    // several executions moving points between each searching
    auto t3 = Clock::now();

    for ( i = 0; i < options.moving_times; ++i )
    {
        for ( auto &p : v )
        {
            p[0] += 0.1*options.scale; /* change cell about 10% of points */
        }

        domain.moved();

        for ( j = 0; j < options.looking_for_number; ++j )
        {
            Point3d p;
            size_t number = find_neighborhood(p, options.radius, near_p, domain);
        }
    }

    auto t4 = Clock::now();

    duration<double> span1 = duration_cast<duration<double> >(t2 - t1);
    duration<double> span2 = duration_cast<duration<double> >(t3 - t2);
    duration<double> span3 = duration_cast<duration<double> >(t4 - t3);

    std::cerr << "Inserting " << options.point_number << " points " << span1.count() << " seconds.\n";
    std::cerr << "Find neighborhoods for " << options.looking_for_number << " points " << span2.count() << " seconds.\n";
    std::cerr << "Moving " << options.moving_times << " times and finding neighborhoods for " << options.looking_for_number
              << " points " << span3.count() << " seconds.\n";
    std::cout << "---- Performance Test ends\n";
}

void test_performance_apply()
{
    std::cout << "---- Begin Performance Test for applying to neighbors\n";
    typedef std::chrono::high_resolution_clock Clock;
    using namespace std::chrono;
    srand(options.random_seed);
    Neighborhood domain(options.scale);
    int i;

    std::vector<Point3d> v(options.point_number);

    auto t1 = Clock::now();

    domain.insert( v.begin(), v.end() );
    domain.print_size(std::cout);

    auto t2 = Clock::now();

    // Find point without moving
    int j;
    for ( j = 0; j < options.looking_for_number; ++j )
    {
        Point3d p;
        size_t number = domain.apply2neighbors(p, options.radius);
    }

    // several executions moving points between each searching
    auto t3 = Clock::now();

    for ( i = 0; i < options.moving_times; ++i )
    {
        for ( auto &p : v )
        {
            p[0] += 0.1*options.scale; /* change cell about 10% of points */
        }

        domain.moved();

        for ( j = 0; j < options.looking_for_number; ++j )
        {
            Point3d p;
            size_t number = domain.apply2neighbors(p, options.radius);
        }
    }

    auto t4 = Clock::now();

    duration<double> span1 = duration_cast<duration<double> >(t2 - t1);
    duration<double> span2 = duration_cast<duration<double> >(t3 - t2);
    duration<double> span3 = duration_cast<duration<double> >(t4 - t3);

    std::cerr << "Inserting " << options.point_number << " points " << span1.count() << " seconds.\n";
    std::cerr << "Find neighborhoods for " << options.looking_for_number << " points " << span2.count() << " seconds.\n";
    std::cerr << "Moving " << options.moving_times << " times and finding neighborhoods for " << options.looking_for_number
              << " points " << span3.count() << " seconds.\n";
    std::cout << "---- Performance Test for applying to neighbors ends\n";
}

void test_performance_combine()
{
    std::cout << "---- Begin Performance Test for combining neighborhood\n";
    typedef std::chrono::high_resolution_clock Clock;
    using namespace std::chrono;
    srand(options.random_seed);
    C_Neighborhood domain(options.scale);
    int i, j;

    std::vector<Point3d> v(options.point_number);
    std::vector<Point3d> w(options.looking_for_number);


    auto t1 = Clock::now();

    domain.insert( v.begin(), v.end() );
    domain.insert_look_for( w.begin(), w.end() );
    domain.print_size(std::cout);

    auto t2 = Clock::now();

    // Find point without moving
    domain.apply(options.radius);

    // several executions moving points between each searching
    auto t3 = Clock::now();

    for ( i = 0; i < options.moving_times; ++i )
    {
        for ( auto &p : v )
        {
            p[0] += 0.1*options.scale; /* change cell about 10% of points */
        }

        domain.moved();

        domain.apply(options.radius);
    }

    auto t4 = Clock::now();

    duration<double> span1 = duration_cast<duration<double> >(t2 - t1);
    duration<double> span2 = duration_cast<duration<double> >(t3 - t2);
    duration<double> span3 = duration_cast<duration<double> >(t4 - t3);

    std::cerr << "Inserting " << options.point_number << " points " << span1.count() << " seconds.\n";
    std::cerr << "Find neighborhoods for " << options.looking_for_number << " points " << span2.count() << " seconds.\n";
    std::cerr << "Moving " << options.moving_times << " times and finding neighborhoods for " << options.looking_for_number
              << " points " << span3.count() << " seconds.\n";
    std::cout << "---- Performance Test for applying to neighbors ends\n";
}

void show_point(const std::vector<Point3d> &v, const Neighborhood& neig, int label)
{
    std::cout << "Point in vector: " << v[label] << "\n";

    for ( const auto& a_pair : neig.domain )
    {
        for ( const auto& details_point : a_pair.second )
        {
            if ( details_point.get_point().label == label )
            {
                std::cout << "Center: " << details_point.get_tessel() << "\n";
                std::cout << "Point: " << details_point.get_point() << "\n";
                std::cout << "Cell is " << a_pair.first << "\n";
            }
        }
    }
}

void show_point(const std::vector<Point3d*> &v, const Neighborhood& neig, int label)
{
    std::cout << "Point in vector: " << *(v[label]) << "\n";

    for ( const auto& a_pair : neig.domain )
    {
        for ( const auto& details_point : a_pair.second )
        {
            if ( details_point.get_point().label == label )
            {
                std::cout << "Center: " << details_point.get_tessel() << "\n";
                std::cout << "Point: " << details_point.get_point() << "\n";
                std::cout << "Cell is " << a_pair.first << "\n";
            }
        }
    }
}

void testing()
{
    int j;
    int fail = 0;
    RefPointVector near_p(100);
    std::vector<Point3d> v(options.point_number);
    Neighborhood domain(options.scale);
    domain.insert( v.begin(), v.end() );

    std::cerr << "---- Begin Regression Test\n";
    for ( j = 0; j < options.looking_for_number; ++j )
    {
        Point3d p;
        size_t number = find_neighborhood(p, options.radius, near_p, domain);
        near_p.resize(number);
        fail = regression_test(p, options.radius, near_p, v);
    }
    std::cerr << "---- Regression Test ends\n";

    std::cerr << "\n";
    std::cerr << "---- Begin Regression Test moving Points\n";
    for ( j = 0; j < options.moving_times; ++j )
    {
        for ( auto &p : v )
        {
            p[1] += 0.2;
        }

        domain.moved();

        Point3d p;
        size_t number = find_neighborhood(p, options.radius, near_p, domain);
        near_p.resize(number);
        if ( regression_test(p, options.radius, near_p, v) )
        {
            fail = 1;
        }
    }

    std::cerr << "---- Regression Test moving Points ends\n";

    if ( fail )
    {
        char show;
        int label;
        std::cout << "Keys: p to show point, d to show domain and s to show size\n";
        while ( (scanf(" %c", &show) == 1) && (show != 'q') )
        {
            switch ( show )
            {
            case 'p':
                if ( scanf(" %i", &label) == 1 )
                {
                    show_point(v, domain, label);
                }
                break;
            case 'd':
                domain.print(std::cout);
                break;
            case 's':
                domain.print_size(std::cout);
                break;
            }
        }
    }
}

void testing_insert_pointers()
{
    int j;
    int fail = 0;
    RefPointVector near_p(100);
    std::vector<Point3d*> v(options.point_number);
    for ( j = 0; j < options.point_number; ++j )
    {
        v[j] = new Point3d;
    }

    Neighborhood domain(options.scale);
    domain.insert_pointers( v.begin(), v.end() );

    std::cerr << "---- Begin Regression Test using Pointer Inserter\n";
    for ( j = 0; j < options.looking_for_number; ++j )
    {
        Point3d p;
        size_t number = find_neighborhood(p, options.radius, near_p, domain);
        near_p.resize(number);
        fail = regression_test_pointers(p, options.radius, near_p, v);
    }
    std::cerr << "---- Regression Test  using Pointer Inserter ends\n";

    std::cerr << "\n";
    std::cerr << "---- Begin Regression Test moving Points using Pointer Inserter\n";
    for ( j = 0; j < options.moving_times; ++j )
    {
        for ( auto *p : v )
        {
            (*p)[1] += 0.2;
        }

        domain.moved();

        Point3d p;
        size_t number = find_neighborhood(p, options.radius, near_p, domain);
        near_p.resize(number);
        if ( regression_test_pointers(p, options.radius, near_p, v) )
        {
            fail = 1;
        }
    }

    std::cerr << "---- Regression Test moving Points using Pointer Inserter ends\n";

    if ( fail )
    {
        char show;
        int label;
        std::cout << "Keys: p to show point, d to show domain and s to show size\n";
        while ( (scanf(" %c", &show) == 1) && (show != 'q') )
        {
            switch ( show )
            {
            case 'p':
                if ( scanf(" %i", &label) == 1 )
                {
                    show_point(v, domain, label);
                }
                break;
            case 'd':
                domain.print(std::cout);
                break;
            case 's':
                domain.print_size(std::cout);
                break;
            }
        }
    }

    for ( j = 0; j < options.point_number; ++j )
    {
        delete v[j];
    }
}

void testing_apply2neighbour()
{
    int j;
    std::vector<Point3d> v(options.point_number);
    Neighborhood domain(options.scale);
    domain.insert( v.begin(), v.end() );

    std::cerr << "---- Begin Regression Test for applying to neighbors\n";
    for ( j = 0; j < options.looking_for_number; ++j )
    {
        Point3d p;
        size_t number = domain.apply2neighbors(p, options.radius);

        Point3d q(p.v);
        q.number = 0;
        for ( const Point3d &x: v )
        {
            if ( x.near2(q, options.radius*options.radius) )
            {
                x.apply(q);
            }
        }

        if ( p.number != q.number )
        {
            std::cerr << "Error these points should have same number: \n" << p << "\n" << q << "\n";
        }

        if ( options.verbose )
        {
            std::cout << "p: " << p << "\n" << " q: " << q << "\n";
        }
    }

    if ( options.verbose )
    {
        domain.print_size(std::cout);
    }

    std::cerr << "---- Regression Test for applying to neighbors ends\n";
}

void testing_combine()
{
    std::cerr << "---- Begin Regression Test for combine neighborhood\n";

    int j, fail = 0;

    std::vector<Point3d> v(options.point_number);
    std::vector<Point3d> w(options.looking_for_number);
    std::vector<Point3d> w_copied;


    for ( const auto& p : w )
    {
        w_copied.push_back( p.v );
        w_copied.back().number = 0;
    }

    C_Neighborhood domain(options.scale);
    domain.insert( v.begin(), v.end() );
    domain.insert_look_for( w.begin(), w.end() );

    domain.apply(options.radius);

    for ( Point3d &p : w_copied )
    {
        size_t number = domain.apply2neighbors(p, options.radius);
    }

    for ( j = 0; j < options.looking_for_number; ++j )
    {
        if ( w[j].number != w_copied[j].number)
        {
            std::cerr << "Error these points should have same number: \n" << w[j] << "\n" << w_copied[j] << "\n";
        }
    }

    std::cerr << "---- Regression Test for combine neighborhood ends\n";
}

void apply_functor(const Point3d& y, Point3d& x)
{
    x.number += y.label;
//#ifdef __COUNTING__
//    ++number_of_apply;
//#endif // __COUNTING__
}

void test_apply_functor_and_algorithm()
{
    std::cerr << "---- Begin Regression Test for combine neighborhood using Apply Functor and near2 as Lambda\n";

    int j, fail = 0;

    std::vector<Point3d> v(options.point_number);
    std::vector<Point3d> w(options.looking_for_number);
    std::vector<Point3d> w_copied;


    for ( const auto& p : w )
    {
        w_copied.push_back( p.v );
        w_copied.back().number = 0;
    }

    C_Neighborhood domain(options.scale);
    domain.insert( v.begin(), v.end() );
    domain.insert_look_for( w.begin(), w.end() );
    double radius2 = options.radius*options.radius;

    apply2neighbors( domain, options.radius, apply_functor,
        [radius2](const Point3d& y, const Point3d& x)
        {
            return y.near2(x, radius2);
        } );

    for ( Point3d &p : w_copied )
    {
        size_t number = domain.apply2neighbors(p, options.radius);
    }

    for ( j = 0; j < options.looking_for_number; ++j )
    {
        if ( w[j].number != w_copied[j].number)
        {
            std::cerr << "Error these points should have same number: \n" << w[j] << "\n" << w_copied[j] << "\n";
        }
    }

    std::cerr << "---- Regression Test for combine neighborhood using Apply Functor and near2 as Lambda ends\n";
}

void test_lambda_and_algorithm()
{
    std::cerr << "---- Begin Regression Test for combine neighborhood using Lambdas\n";

    int j, fail = 0;

    std::vector<Point3d> v(options.point_number);
    std::vector<Point3d> w(options.looking_for_number);
    std::vector<Point3d> w_copied;


    for ( const auto& p : w )
    {
        w_copied.push_back( p.v );
        w_copied.back().number = 0;
    }

    C_Neighborhood domain(options.scale);
    domain.insert( v.begin(), v.end() );
    domain.insert_look_for( w.begin(), w.end() );

    double radius2 = options.radius*options.radius;

    apply2neighbors( domain, options.radius,
        [](const Point3d& y, Point3d& x)
        {
            x.number += y.label;
        },
        [radius2](const Point3d& y, const Point3d& x)
        {
            return y.near2(x, radius2);
        } );

    for ( Point3d &p : w_copied )
    {
        size_t number = domain.apply2neighbors(p, options.radius);
    }

    for ( j = 0; j < options.looking_for_number; ++j )
    {
        if ( w[j].number != w_copied[j].number)
        {
            std::cerr << "Error these points should have same number: \n" << w[j] << "\n" << w_copied[j] << "\n";
        }
    }

    std::cerr << "---- Regression Test for combine neighborhood using Lambdas ends\n";
}

void other_test_1()
{
    srand(options.random_seed);

    RefPointVector near_p(10);
    std::vector<Point3d> v(options.point_number);
    Neighborhood domain(options.scale);
    domain.insert( v.begin(), v.end() );

    domain.print_size(std::cout);
}

void other_test_2()
{
    typedef std::chrono::high_resolution_clock Clock;
    using namespace std::chrono;
    auto t1 = Clock::now();
    int j;

    for ( j = 0; j < options.point_number; ++j )
    {
        Point3d p;
        point<Point3d> v( 0.5, &p );
    }

    auto t2 = Clock::now();

    duration<double> span1 = duration_cast<duration<double> >(t2 - t1);

    std::cerr << "Find tessel for " << options.point_number << " points " << span1.count() << " seconds.\n";
}

int main(int argc, char** argv)
{
    options.load(argc, argv);

    if ( 1 )
    {
        testing();
        testing_apply2neighbour();

        test_performance();
        test_performance_apply();
        test_performance_combine();

        testing_insert_pointers();

        test_apply_functor_and_algorithm();
        test_lambda_and_algorithm();

        testing_combine();
    }
    else
    {
        other_test_1();
        other_test_2();
    }

    return 0;
}
