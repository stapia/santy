/****************************************************************************
 * (c) 2013 Copyright Santiago Tapia-Fernández (UPM)
 *
 * This file is part of Santy Library.
 *
 *     Santy Library is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Santy Library is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Santy Library.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#include "program_options.hpp"

#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>

void program_options::load(int argc, char** argv)
{
    verbose = 0;
    random_seed = 4534;

    point_number = 100000;

    looking_for_number = 100;
    radius = 1.5;
    scale = 3.0001;

    moving_times = 10;

    int c;

    struct option my_long_options[] =
    {
        /* These options set a flag. */
        {"verbose", no_argument,       &verbose, 1},
        //{"brief",   no_argument,       &verbose_flag, 0},
        /* These options don’t set a flag.
           We distinguish them by their indices. */
        {"scale",       required_argument, 0, 'z' },
        {"radius",     required_argument, 0, 'r' },
        {"seed",       required_argument, 0, 's' },
        {"points",     required_argument, 0, 'p' },
        {"looking_for",required_argument, 0, 'f' },
        {"moving",     required_argument, 0, 'm' },
        {0, 0, 0, 0}
    };

    int option_index = 0;

    while ( (c = getopt_long (argc, argv, "s:r:p:m:z:", my_long_options, &option_index) ) != -1 )
    {
        switch (c)
        {
        case 0:
            /* If this option set a flag, do nothing else now. */
            if (my_long_options[option_index].flag != 0)
                break;
            printf ("option %s", my_long_options[option_index].name);
            if (optarg)
                printf (" with arg %s", optarg);
            printf ("\n");
            break;

        case 's':
            random_seed = atoi(optarg);
            printf ("random seed set to %i\n", random_seed);
            break;

        case 'r':
            radius = atof(optarg);
            printf ("radius set to %f\n", radius);
            break;

        case 'z':
            scale = atof(optarg);
            printf ("scale set to %f\n", scale);
            break;

        case 'p':
            point_number = atoi(optarg);
            printf ("points number set to %i\n", point_number);
            break;

        case 'f':
            looking_for_number = atoi(optarg);
            printf ("points number (to look for) set to %i\n", looking_for_number);
            break;

        case 'm':
            moving_times = atoi(optarg);
            printf ("moving times set to %i\n", moving_times);
            break;

        case '?':
            /* getopt_long already printed an error message. */
            break;

        default:
            abort ();
        }
    }

    /* Print any remaining command line arguments (not options). */
    if (optind < argc)
    {
        printf ("non-option ARGV-elements: ");
        while (optind < argc)
            printf ("%s ", argv[optind++]);
        putchar ('\n');
    }
}


