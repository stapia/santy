/****************************************************************************
 * (c) 2013 Copyright Santiago Tapia-Fernández (UPM)
 *
 * This file is part of Santy Library.
 *
 *     Santy Library is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Santy Library is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Santy Library.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#ifndef EXAMPLE_POINT_HPP_624624564WY56
#define EXAMPLE_POINT_HPP_624624564WY56

#define __COUNTING__

#include <cmath>
#include <cstdlib>
#include <cstring>
#include <iostream>

static int next_label = 0;

struct Point3d
{
    int number;
    int label;
    double v[3];
    static double range;
#ifdef __COUNTING__
    static unsigned number_of_apply;
    static unsigned number_of_distance;
#endif // __COUNTING__

    Point3d()
        : label(next_label)
        , number(0)
    {
        int i;
        for ( i = 0; i < 3; ++i )
        {
            v[i] = rand() * range / RAND_MAX;
        }
        ++next_label;
    }

    Point3d(const double* w)
        : label(next_label)
        , number(0)
    {
        memcpy(v, w, 3*sizeof(double));
        ++next_label;
    }

    inline double& operator[] (int i)
    {
        return v[i];
    }

    inline const double& operator[] (int i) const
    {
        return v[i];
    }

    inline bool operator< (const Point3d& rhs) const
    {
        return label < rhs.label;
    }

    inline bool near2(const Point3d& v2, double dist2) const
    {
        double aux = 0; int i;
        for ( i = 0; i < 3; ++i )
        {
            aux += ( v[i] - v2[i] ) * ( v[i] - v2[i] );
        }
#ifdef __COUNTING__
        ++number_of_distance;
#endif // __COUNTING__
        return aux < dist2;
    }

    inline void apply(Point3d& x) const
    {
        x.number += label;
#ifdef __COUNTING__
        ++number_of_apply;
#endif // __COUNTING__
    }
};

inline std::ostream& operator<< (std::ostream& out, const Point3d& p)
{
    out << " label: " << p.label << ", number: " << p.number <<", ( " << p[0] << " , " << p[1] << " , " << p[2] << " ) ";
    return out;
}

inline double distance(const Point3d& v1, const Point3d& v2)
{
    double aux = 0;
    int i;
    for ( i = 0; i < 3; ++i )
    {
        aux += ( v1[i] - v2[i] ) * ( v1[i] - v2[i] );
    }
    return sqrt(aux);
}

#endif // EXAMPLE_POINT_HPP_624624564WY56
